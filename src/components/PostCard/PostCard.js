import React from "react"
import { format } from "date-fns"
import { Box, Link, Card, Heading, Text } from "theme-ui"
import { BadgesGroup } from "../BadgesGroup"
import Img from "gatsby-image"

export const PostCard = ({
  index,
  title,
  tags,
  date,
  slug,
  featuredImage,
  excerpt,
  pinned,
}) => {
  return (
    <Box
      key={index}
      sx={{
        display: "flex",
        mb: 3,
        maxWidth: ["100%", "100%", "50%", "50%"],
        width: ["100%", "100%", "50%", "50%"],
      }}
    >
      <Link
        href={slug}
        sx={{
          textDecoration: "none",
          display: "flex",
          flex: "1 1 auto",
          flexDirecmtion: "column",
          m: theme => `0px ${theme.space[2]}px`,
          minHeight: "1px",
        }}
      >
        <Card
          sx={{
            display: "flex",
            flex: "1 1 auto",
            flexDirection: "column",
            minHeight: "1px",
          }}
        >
          {featuredImage && (
            <Box sx={{ minHeight: "1px" }}>
              <Img
                fluid={featuredImage.childImageSharp.fluid}
                alt={featuredImage.childImageSharp.fluid.originalName}
              />
            </Box>
          )}
          <Box
            sx={{
              p: 3,
            }}
          >
            <Text sx={{ fontSize: 0, mb: 1, color: "muted" }}>
              {format(new Date(date), "yyyy-MM-dd")}
            </Text>
            <Heading
              variant="styles.h4"
              sx={{ color: pinned ? "primary" : "text" }}
            >
              {title}
            </Heading>
            <BadgesGroup
              arr={tags}
              color1="secondary"
              color2="primary"
            ></BadgesGroup>
            <Text sx={{ mb: 1, color: "text" }}>{excerpt}</Text>
          </Box>
          <Box sx={{ p: 3, textDecoration: "underline" }}>
            <Text>View Post</Text>
          </Box>
        </Card>
      </Link>
    </Box>
  )
}
