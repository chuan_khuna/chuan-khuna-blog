import React from "react"
import { Badge } from "theme-ui"
import { mix } from "@theme-ui/color"
import { Fragment } from "react"

export const BadgesGroup = ({ arr, color1='primary', color2='secondary' }) => {
  return (
    <Fragment>
      {arr.map((elem, index) => {
        return (
          <Badge
            key={index}
            variant={color1}
            sx={{
              mr: 2,
              mb: 2,
              color: mix(color2, color1, `${index / arr.length}`),
              borderColor: mix(color2, color1, `${index / arr.length}`),
            }}
          >
            {elem}
          </Badge>
        )
      })}
    </Fragment>
  )
}
