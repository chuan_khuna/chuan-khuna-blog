import React from "react"
import { Badge, Link, Text } from "theme-ui"
import { mix } from "@theme-ui/color"
import { Fragment } from "react"

export const References = ({
  dictionary,
  color1 = "primary",
  color2 = "secondary",
}) => {
  const dict_length = Object.keys(dictionary).length
  return (
    <Fragment>
      <Text variant="muted" sx={{ mt: "5em", mb: 1 }}>
        {" "}
        References{" "}
      </Text>
      {Object.entries(dictionary).map((kv, index) => {
        return (
          <Badge
            key={index}
            variant={color1}
            sx={{
              mb: 2,
              mr: 2,
              color: mix(color2, color1, `${index / dict_length}`),
              borderColor: mix(color2, color1, `${index / dict_length}`),
              fontSize: "0.8em",
            }}
          >
            {kv[1] ? (
              <Link
                href={kv[1]}
                sx={{
                  textDecoration: "none",
                  color: mix(color2, color1, `${index / dict_length}`),
                }}
              >
                {kv[0]}
              </Link>
            ) : (
              `${kv[0]}`
            )}
          </Badge>
        )
      })}
    </Fragment>
  )
}
