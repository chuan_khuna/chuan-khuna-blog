import React from "react"
import { Box, Link, Card, Heading, Text } from "theme-ui"
import { BadgesGroup } from "../BadgesGroup"

export const SawanizedCard = ({ dict }) => {
  const color1 = "primary"
  const color2 = "text"
  return (
    <Box
      sx={{
        display: "flex",
        mb: 3,
        maxWidth: ["100%", "100%", "50%", "50%"],
        width: ["100%", "100%", "50%", "50%"],
      }}
    >
      <Box
        sx={{
          textDecoration: "none",
          display: "flex",
          flex: "1 1 auto",
          flexDirecmtion: "column",
          m: theme => `0px ${theme.space[2]}px`,
          minHeight: "1px",
        }}
      >
        <Card
          sx={{
            display: "flex",
            flex: "1 1 auto",
            flexDirection: "column",
            minHeight: "1px",
          }}
        >
          <Box sx={{ p: 3 }}>
            <Box>
              <Text sx={{ fontSize: 0, mb: 2, color: "muted" }}>
                {dict.album}
              </Text>
              <Heading
                variant="styles.h4"
                sx={{
                  pt: 2,
                  pb: 2,
                  mt: 2,
                  mb: 2,
                }}
              >
                {dict.trackname}
              </Heading>
            </Box>
            <Box sx={{ mb: 3 }}>
              <BadgesGroup arr={dict.meaning} color1={color1} color2={color2} />
            </Box>
            {dict.explain.map((elem, index) => {
              return (
                <Text sx={{ mb: 1, fontSize: 1 }} variant="muted">
                  {elem}
                </Text>
              )
            })}
            {dict.ref && (
              <Box sx={{ mt: 3, textDecoration: "underline" }}>
                <Link href={dict.ref}>
                  <Text sx={{ fontSize: "0.8em" }}>Reference</Text>
                </Link>
              </Box>
            )}
          </Box>
        </Card>
      </Box>
    </Box>
  )
}
