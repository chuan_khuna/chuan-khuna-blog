/** @jsx jsx */
import { useColorMode, Button, Flex, jsx, Box, Divider, Text } from "theme-ui"

export default function ThemeSwitch() {
  const [colorMode, setColorMode] = useColorMode()

  return (
    <Box
      sx={{
        alignItems: "center",
        display: "flex",
        height: theme => `${theme.space[5]}px`,
        justifyContent: ["flex-start", "flex-start", "flex-start", "flex-end"],
        overFlow: "hidden",
        mt: 4,
        px: 2,
      }}
    >
      <Divider />
      <Button
        variant="muted"
        sx={{ backgroundColor: "primary" }}
        onClick={e => {
          setColorMode(colorMode === "default" ? "dark" : "default")
        }}
      >
        <Flex sx={{ alignItems: "center", justifyContent: "center" }}>
          <Text sx={{ mr: 1 }}>
            {colorMode === "default" ? "Darken" : "Lighten"}
          </Text>
        </Flex>
      </Button>
    </Box>
  )
}
