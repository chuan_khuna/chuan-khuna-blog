import codeTheme from "@theme-ui/prism/presets/oceanic-next.json"
import baseTheme from "@chuan-khuna/gatsby-theme-terminal/src/gatsby-plugin-theme-ui"

export default {
  ...baseTheme,
  fonts: {
    body: '"OperatorMonoBook", "Sarabun"',
    heading: '"OperatorMonoBook", "Sarabun"',
    mono: '"DankMono", "Sarabun"',
  },
  fontSizes: [14, 16, 18, 22, 24],
  colors: {
    ...baseTheme.colors,
    text: "#1b262c",
    muted: "#311d3f",
    primary: "#202f66",
    secondary: "#ff7048",
    success: "#48ADA9",
    background: "#f9f7f7",
    surface: "#EDE8E8",
    modes: {
      dark: {
        text: "#eaeaea",
        muted: "#B5B7CA",
        primary: "#ff2e63",
        secondary: "#08d9d6",
        success: "#27FB6B",
        background: "#0e1014",
        surface: "#21222e",
      },
    },
  },
  styles: {
    ...baseTheme.styles,
    p: {
      code: {
        ...baseTheme.styles.p.code,
        color: "inherit",
        fontSize: "0.8em",
      },
    },
    li: {
      code: {
        ...baseTheme.styles.p.code,
        color: "inherit",
        fontSize: "0.8em",
      },
    },
    pre: {
      ...baseTheme.styles.pre,
      ...codeTheme,
      fontSize: "0.9em",
    },
    h1: {
      ...baseTheme.styles.h1,
      fontSize: 3,
    },
    h2: { ...baseTheme.styles.h2, fontSize: 2 },
    h3: { ...baseTheme.styles.h3, fontSize: 2 },
  },
  links: {
    ...baseTheme.links,
    nav: {
      ...baseTheme.links.nav,
      '&[aria-current="page"]': {
        color: "secondary",
        pointerEvents: "none",
        fontSize: "1.1em",
      },
    },
  },
}
