const path = require('path')

module.exports = {
  siteMetadata: {
    name: `CHUAN KHUNA`,
    description: `A blog and portfolio site for Chuan Khuna.`,
    keywords: [`blog`, `gatsby-theme`],
    siteUrl: `https://chuan-khuna.netlify.app/`,
    siteImage: `https://chuan-khuna.netlify.app/images/site_image.png`,
    profileImage: `https://chuan-khuna.netlify.app/images/site_image.png`,
    lang: `en`,
    config: {
      sidebarWidth: 280,
    },
  },
  plugins: [
    {
      resolve: `@chuan-khuna/gatsby-theme-terminal`,
      options: {
        source: [`posts`, `notes`],
      },
    },
    {
      resolve: `gatsby-plugin-alias-imports`,
      options: {
        alias: {"@components": path.resolve(__dirname, 'src/components')},
        extensions: [`js`]
      }
    }
  ],
}
